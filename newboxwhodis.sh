#!/bin/bash
cd ~
echo '[*] Supply sudo password if necessary...'
sudo apt update
sudo apt install git wfuzz terminator ffuf golang-go python3 -y
mkdir Apps
cd Apps
echo '[*] Installing seclists...'
git clone https://github.com/danielmiessler/SecLists.git
echo '[*] Installing impacket...'
git clone https://github.com/SecureAuthCorp/impacket.git
echo '[*] Installing reconftw...'
git clone https://github.com/six2dez/reconftw
cd reconftw/
./install.sh
echo '[*] Installing axiom...'
bash <(curl -s https://raw.githubusercontent.com/pry0cc/axiom/master/interact/axiom-configure)
cd ~
echo '[*] Installing hakrawler...'
go get github.com/hakluke/hakrawler
echo '[*] Installing bbrf...'
pip install bbrf
echo '[*] Setting up symlinks...'
ln -s /usr/share/wordlists .
sudo ln -s ~/Apps/SecLists /usr/share/wordlists/seclists
ln -s /mnt/hgfs/Work .
ln -s /mnt/hgfs/kalishared .
echo '[*] Adding aliases...'
echo 'alias httpserver="sudo python3 -m http.server 80"' >> ~/.bashrc
echo "[*] We're done! Happy hacking!"
